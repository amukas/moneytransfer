package am.mt.core.services.impl


import am.mt.core.domain.Account
import am.mt.core.domain.Currency
import am.mt.core.domain.MoneyTransferStatus
import am.mt.core.services.AccountManagementService
import am.mt.core.services.IdGenerator
import am.mt.core.services.MoneyTransferService
import am.mt.core.services.TransfersRepository
import am.mt.core.utils.WithAmountConversion
import spock.lang.Specification

import java.util.concurrent.Executors
import java.util.concurrent.Future
import java.util.concurrent.atomic.AtomicInteger

import static am.mt.core.domain.Currency.USD

class MoneyTransferServiceImplTest extends Specification implements WithAmountConversion {

    IdGenerator idGenerator = new IdGeneratorImpl()
    AccountManagementService accountManagement = new AccountManagementInMemoryService(idGenerator)
    TransfersRepository transfersRepository = new InMemoryTransfersRepository()
    MoneyTransferService sut = new MoneyTransferServiceImpl(idGenerator, accountManagement, transfersRepository)

    def "simple money transfer"() {
        given:
        Account from = Account.builder()
                .id(idGenerator.nextId())
                .balance(USD, toRawAmount(100.00d))
                .build()
        Account to = Account.builder()
                .id(idGenerator.nextId())
                .build()
        accountManagement.updateAccount(from, to)
        Currency currencyToTransfer = USD
        long amountToTransfer = toRawAmount(30.0d)

        when:
        def transfer = sut.transfer(from.id, to.id, currencyToTransfer, amountToTransfer)

        then:
        with(transfer) {
            status == MoneyTransferStatus.SUCCEED
            fromAccountId == from.id
            toAccountId == to.id
        }
        with(accountManagement.findById(from.id)) {
            fromRawAmount(balanceFor(currencyToTransfer)) == 70.0d
        }
        with(accountManagement.findById(to.id)) {
            fromRawAmount(balanceFor(currencyToTransfer)) == 30.0d
        }
    }

    def "money transfer is thread safe"() {
        given:
        def r = new Random()
        int accountsCount = 10 // small number of accounts to have more chances for potential deadlock/concurrency issue
        int threadsCount = 16
        int transfersCount = 1_000_000
        long initialRawBalance = toRawAmount(1000.0)
        def currency = USD

        List<Account> accounts = new ArrayList<>(accountsCount)
        accountsCount.times { i ->
            def account = Account.builder()
                    .id(idGenerator.nextId())
                    .balance(currency, initialRawBalance)
                    .build()
            accounts.add(account)
            accountManagement.updateAccount(account)
        }
        def executors = Executors.newFixedThreadPool(threadsCount)
        List<Future> tasks = new ArrayList<>(transfersCount)
        def nextRandomAccountId = {
            ->
            def nextAccountIndex = Math.abs(r.nextInt()) % accountsCount
            accounts[nextAccountIndex].id
        }
        def successfulTransfers = new AtomicInteger()

        when:
        transfersCount.times { i ->
            def task = executors.submit {
                ->
                def transferred = false
                while (!transferred) {
                    def fromAccountId = nextRandomAccountId()
                    def toAccountId = nextRandomAccountId()
                    if (fromAccountId != toAccountId) {
                        def fromAccount = accountManagement.findById(fromAccountId)
                        def balance = fromAccount.balanceFor(currency)
                        def nextTransferSize = Math.abs(r.nextInt(5))
                        if (balance >= nextTransferSize) {
                            def transfer = sut.transfer(fromAccount.id, toAccountId, currency, nextTransferSize)
                            if (transfer.status == MoneyTransferStatus.SUCCEED) {
                                transferred = true
                                successfulTransfers.incrementAndGet()
                            }
                        }
                    }
                }
            }
            tasks.add(task)
        }
        tasks.forEach { task ->
            task.get() // wait for completion
        }
        executors.shutdownNow()

        then:
        assert successfulTransfers.get() == transfersCount
        def sumBalanceAfterTransfers = accounts.sum { each ->
            def account = accountManagement.findById(each.id) // to check actual persisted state
            account.balanceFor(currency)
        }
        assert sumBalanceAfterTransfers == accountsCount * initialRawBalance
    }
}
