package am.mt


import am.mt.core.domain.Account
import am.mt.core.domain.Currency
import am.mt.core.domain.MoneyTransfer
import am.mt.core.domain.MoneyTransferStatus
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import lombok.Data
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import spark.Spark
import spock.lang.Shared
import spock.lang.Specification

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST

class MoneyTransferAcceptanceTest extends Specification implements WithTestHelpers {

    @Shared
    AccountsApi accountsApi
    @Shared
    TransfersApi transfersApi

    void setupSpec() {
        def port = availablePort()
        new MoneyTransferApplication().run(port)

        Gson mapper = new GsonBuilder()
                .setLenient()
                .create();

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl("http://localhost:${port}/")
                .addConverterFactory(GsonConverterFactory.create(mapper))
                .build();
        accountsApi = retrofit.create(AccountsApi.class)
        transfersApi = retrofit.create(TransfersApi.class)
    }

    void cleanupSpec() {
        Spark.stop()
    }

    def "simple money transfer"() {
        given:
        def currency = Currency.USD

        // accounts setup
        def john = accountsApi.createAccount(new AccountCreateReq(accountHolder: "John Doe")).execute().body()
        john = accountsApi.topUpAccount(john.id, new TopUpReq(currency: currency, amount: 100)).execute().body()
        def jane = accountsApi.createAccount(new AccountCreateReq(accountHolder: "Jane Doe")).execute().body()

        when:
        def transfer = transfersApi.createTransfer(new TransferCreateReq(
                fromAccountId: john.id,
                toAccountId: jane.id,
                currency: currency,
                amount: 50
        )).execute().body()

        then:
        transfer.status == MoneyTransferStatus.SUCCEED
        accountsApi.findAccount(john.id).execute().body().balanceFor(currency) == 50
        accountsApi.findAccount(jane.id).execute().body().balanceFor(currency) == 50
    }

    def "create account request validation: fromAccountId doesn't exist"() {
        expect:
        with(performTransfer { r ->
            r.fromAccountId = "100"
        }) {
            code() == SC_BAD_REQUEST
            new String(errorBody().bytes()).contains("[fromAccountId] is not valid")
        }
    }

    def "create account request validation: fromAccountId not integer"() {
        expect:
        with(performTransfer { r ->
            r.fromAccountId = "abc"
        }) {
            code() == SC_BAD_REQUEST
            new String(errorBody().bytes()).contains("[fromAccountId] is not valid")
        }
    }

    def "create account request validation: toAccountId doesn't exist"() {
        expect:
        with(performTransfer { r ->
            r.toAccountId = "100"
        }) {
            code() == SC_BAD_REQUEST
            new String(errorBody().bytes()).contains("[toAccountId] is not valid")
        }
    }

    def "create account request validation: toAccountId not integer"() {
        expect:
        with(performTransfer { r ->
            r.toAccountId = "abc"
        }) {
            code() == SC_BAD_REQUEST
            new String(errorBody().bytes()).contains("[toAccountId] is not valid")
        }
    }

    def "create account request validation: not supported currency"() {
        expect:
        with(performTransfer { r ->
            r.currency = "PLN"
        }) {
            code() == SC_BAD_REQUEST
            new String(errorBody().bytes()).contains("[currency] is not valid")
        }
    }

    def "create account request validation: invalid amount"() {
        expect:
        with(performTransfer { r ->
            r.amount = "-1"
        }) {
            code() == SC_BAD_REQUEST
            new String(errorBody().bytes()).contains("[amount] is not valid")
        }
    }

    def "create account request validation: amount non integer"() {
        expect:
        with(performTransfer { r ->
            r.amount = "abc"
        }) {
            code() == SC_BAD_REQUEST
            new String(errorBody().bytes()).contains("[amount] is not valid")
        }
    }

    def performTransfer(reqModifier) {
        def currency = Currency.USD

        // accounts setup
        def john = accountsApi.createAccount(new AccountCreateReq(accountHolder: "John Doe")).execute().body()
        john = accountsApi.topUpAccount(john.id, new TopUpReq(currency: currency, amount: 100)).execute().body()
        def jane = accountsApi.createAccount(new AccountCreateReq(accountHolder: "Jane Doe")).execute().body()

        def req = new TransferCreateReqRaw(
                fromAccountId: john.id,
                toAccountId: jane.id,
                currency: currency,
                amount: 50
        )
        reqModifier(req)

        transfersApi.createTransferRaw(req).execute()
    }

    interface AccountsApi {
        @POST("/accounts")
        Call<Account> createAccount(@Body AccountCreateReq req)

        @GET("/accounts/{id}")
        Call<Account> findAccount(@Path("id") long accountId)


        @POST("/accounts/{id}/top-ups")
        Call<Account> topUpAccount(@Path("id") long accountId, @Body TopUpReq req)
    }

    interface TransfersApi {
        @POST("/transfers")
        Call<MoneyTransfer> createTransfer(@Body TransferCreateReq req)

        @POST("/transfers")
        Call<MoneyTransfer> createTransferRaw(@Body TransferCreateReqRaw req)

        @GET("/transfers/{id}")
        Call<MoneyTransfer> findTransfer(@Path("id") long transferId)
    }

    @Data
    private final static class AccountCreateReq {
        private String accountHolder;
    }

    @Data
    private final static class TransferCreateReq {
        private long fromAccountId;
        private long toAccountId;
        private Currency currency;
        private double amount;
    }

    @Data
    private final static class TransferCreateReqRaw {
        private String fromAccountId;
        private String toAccountId;
        private String currency;
        private String amount;
    }

    @Data
    private final static class TopUpReq {
        private Currency currency;
        private double amount;
    }
}
