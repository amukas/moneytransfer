package am.mt;

import lombok.SneakyThrows;

import java.net.ServerSocket;

public interface WithTestHelpers {

    @SneakyThrows
    default int availablePort() {
        try (ServerSocket socket = new ServerSocket(0)) {
            return socket.getLocalPort();
        }
    }
}
