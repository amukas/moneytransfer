package am.mt.api.rest;

import am.mt.api.rest.common.ErrorResp;
import am.mt.api.rest.common.WithModelValidation;
import am.mt.core.domain.Account;
import am.mt.core.domain.Currency;
import am.mt.core.services.AccountManagementService;
import am.mt.core.utils.WithAmountConversion;
import com.google.gson.Gson;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import spark.Request;
import spark.Response;

import java.util.HashMap;
import java.util.Map;

import static javax.servlet.http.HttpServletResponse.*;
import static spark.Spark.get;
import static spark.Spark.post;

@RequiredArgsConstructor
public class AccountsRestApi implements WithAmountConversion {

    private final Gson mapper;
    private final AccountManagementService accountManagement;

    public void initialize() {
        post("/accounts", this::createAccount);
        get("/accounts/:id", this::findAccount);
        post("/accounts/:id/top-ups", this::topUpAccount);
    }

    private Object createAccount(Request req, Response resp) {
        CreateReq createReq = mapper.fromJson(req.body(), CreateReq.class);
        ErrorResp error = createReq.validate();
        if (error != null) {
            resp.status(SC_BAD_REQUEST);
            return error;
        }

        Account account = accountManagement.createAccount(createReq.accountHolder);
        resp.status(SC_CREATED);
        return AccountResp.fromAccount(account);
    }

    private Object findAccount(Request req, Response resp) {
        FindAccountReq findAccountReq = new FindAccountReq();
        findAccountReq.setId(req.params("id"));
        ErrorResp error = findAccountReq.validate();
        if (error != null) {
            resp.status(SC_BAD_REQUEST);
            return error;
        }

        Account account = accountManagement.findById(findAccountReq.getIdAsLong());
        if (account == null) {
            resp.status(SC_NOT_FOUND);
            return null;
        } else {
            resp.status(SC_OK);
            return AccountResp.fromAccount(account);
        }
    }

    private Object topUpAccount(Request req, Response resp) {
        TopUpReq topUpReq = mapper.fromJson(req.body(), TopUpReq.class);
        topUpReq.setId(req.params("id"));
        ErrorResp error = topUpReq.validate(accountManagement);
        if (error != null) {
            resp.status(SC_BAD_REQUEST);
            return error;
        }

        Account account = accountManagement.topUp(topUpReq.getIdAsLong(), topUpReq.getCurrency(), toRawAmount(topUpReq.getAmountAsDouble()));
        resp.status(SC_CREATED);
        return AccountResp.fromAccount(account);
    }

    // ------

    @Data
    private final static class CreateReq implements WithModelValidation {
        private String accountHolder;

        @SuppressWarnings("unchecked")
        public ErrorResp validate() {
            return validating(this)
                    .nonNull("accountHolder", m -> m.accountHolder)
                    .done();
        }
    }

    @Data
    private final static class FindAccountReq implements WithModelValidation {
        private String id;

        public long getIdAsLong() {
            return Long.parseLong(id);
        }

        @SuppressWarnings("unchecked")
        public ErrorResp validate() {
            return validating(this)
                    .nonNull("id", m -> m.id,
                            v -> v.isValid("id", m -> Long.parseLong(m.id) > 0))
                    .done();
        }
    }

    @SuppressWarnings("InnerClassMayBeStatic")
    @Data
    private final static class TopUpReq implements WithModelValidation {
        private String id;
        private String currency;
        private String amount;

        public long getIdAsLong() {
            return Long.parseLong(id);
        }

        public double getAmountAsDouble() {
            return Double.parseDouble(amount);
        }

        public Currency getCurrency() {
            return Currency.fromCode(currency);
        }

        @SuppressWarnings("unchecked")
        public ErrorResp validate(AccountManagementService accountManagement) {
            return validating(this)
                    .nonNull("id", m -> m.id,
                            v -> v.isValid("id", m -> Long.parseLong(m.id) > 0,
                                    v2 -> v2.isValid("id", m -> accountManagement.accountExists(m.getIdAsLong()))))
                    .nonNull("currency", m -> m.currency,
                            v -> v.isValid("currency", m -> Currency.fromCode(m.currency) != null))
                    .nonNull("amount", m -> m.amount,
                            v -> v.isValid("amount", m -> Double.parseDouble(m.amount) > 0))
                    .done();
        }
    }

    @Data
    @Builder
    private final static class AccountResp {
        private final long id;
        private final String holder;
        private Map<Currency, Double> balances;

        public static AccountResp fromAccount(Account account) {
            Map<Currency, Long> rawBalance = account.getBalances();
            Map<Currency, Double> balance = new HashMap<>(rawBalance.size());
            rawBalance.forEach((c, b) -> balance.put(c, AmountConversion.fromRawAmount(b)));

            return AccountResp.builder()
                    .id(account.getId())
                    .holder(account.getHolder())
                    .balances(balance)
                    .build();
        }
    }
}
