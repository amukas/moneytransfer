package am.mt.api.rest.common;


import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.Singular;

import java.util.List;

@Data
@Builder
public class ErrorResp {
    private final String description;
    @Singular
    private final List<Error> errors;

    @Data
    @RequiredArgsConstructor(staticName = "of")
    public static class Error {
        private final String message;
    }
}
