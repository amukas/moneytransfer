package am.mt.api.rest.common;

import lombok.RequiredArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

public interface WithModelValidation {

    default <T> Validating<T> validating(T model) {
        return new Validating<>(model);
    }

    @RequiredArgsConstructor
    final class Validating<T> {
        private final T model;
        private final List<ErrorResp.Error> errors = new ArrayList<>();

        public <X> Validating<T> nonNull(String name, Function<T, X> extractor, Function<Validating<T>, Validating<T>>... other) {
            X candidate = extractor.apply(model);
            if (candidate == null) {
                this.errors.add(ErrorResp.Error.of("[" + name + "] is not set"));
            }
            if (other != null) {
                for (Function<Validating<T>, Validating<T>> each : other) {
                    each.apply(this);
                }
            }
            return this;
        }

        public Validating<T> isValid(String name, Function<T, Boolean> extractor, Function<Validating<T>, Validating<T>>... other) {
            boolean valid;
            try {
                valid = extractor.apply(model);
            } catch (Exception e) {
                valid = false;
            }
            if (!valid) {
                this.errors.add(ErrorResp.Error.of("[" + name + "] is not valid"));
            }
            if (other != null) {
                for (Function<Validating<T>, Validating<T>> each : other) {
                    each.apply(this);
                }
            }

            return this;
        }

        public ErrorResp done() {
            return errors.isEmpty() ?
                    null :
                    ErrorResp.builder().description("Request is not valid").errors(this.errors).build();
        }
    }
}
