package am.mt.api.rest;

import am.mt.api.rest.common.ErrorResp;
import am.mt.api.rest.common.WithModelValidation;
import am.mt.core.domain.Currency;
import am.mt.core.domain.MoneyTransfer;
import am.mt.core.domain.MoneyTransferStatus;
import am.mt.core.services.AccountManagementService;
import am.mt.core.services.MoneyTransferService;
import am.mt.core.utils.WithAmountConversion;
import com.google.gson.Gson;
import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import spark.Request;
import spark.Response;

import static javax.servlet.http.HttpServletResponse.SC_BAD_REQUEST;
import static javax.servlet.http.HttpServletResponse.SC_CREATED;
import static spark.Spark.get;
import static spark.Spark.post;

@RequiredArgsConstructor
public class TransfersRestApi implements WithAmountConversion {

    private final Gson mapper;
    private final AccountManagementService accountManagement;
    private final MoneyTransferService moneyTransfer;

    public void initialize() {
        post("/transfers", this::createTransfer);
        get("/transfers/:id", this::findTransfer);
    }

    private Object createTransfer(Request req, Response resp) {
        CreateReq createReq = mapper.fromJson(req.body(), CreateReq.class);
        ErrorResp error = createReq.validate(accountManagement);
        if (error != null) {
            resp.status(SC_BAD_REQUEST);
            return error;
        }

        resp.status(SC_CREATED);
        MoneyTransfer transfer = moneyTransfer.transfer(
                createReq.getFromAccountIdAsLong(), createReq.getToAccountIdAsLong(),
                createReq.getCurrency(), toRawAmount(createReq.getAmountAsDouble()));
        return TransferResp.fromAccount(transfer);
    }

    private Object findTransfer(Request req, Response resp) {
        long id = Long.parseLong(req.params("id"));
        MoneyTransfer transfer = moneyTransfer.findById(id);
        return transfer != null ? TransferResp.fromAccount(transfer) : null;
    }

    // ------

    @Data
    private final static class CreateReq implements WithModelValidation {
        private String fromAccountId;
        private String toAccountId;
        private String currency;
        private String amount;

        public long getFromAccountIdAsLong() {
            return Long.parseLong(fromAccountId);
        }

        public long getToAccountIdAsLong() {
            return Long.parseLong(toAccountId);
        }

        public double getAmountAsDouble() {
            return Double.parseDouble(amount);
        }

        public Currency getCurrency() {
            return Currency.fromCode(currency);
        }

        @SuppressWarnings("unchecked")
        public ErrorResp validate(AccountManagementService accountManagement) {
            return validating(this)
                    .nonNull("fromAccountId", m -> m.fromAccountId,
                            v -> v.isValid("fromAccountId", m -> Long.parseLong(m.fromAccountId) > 0,
                                    v2 -> v2.isValid("fromAccountId", m -> accountManagement.accountExists(m.getFromAccountIdAsLong()))))
                    .nonNull("toAccountId", m -> m.toAccountId,
                            v -> v.isValid("toAccountId", m -> Long.parseLong(m.toAccountId) > 0,
                                    v2 -> v2.isValid("toAccountId", m -> accountManagement.accountExists(m.getToAccountIdAsLong()))))
                    .nonNull("currency", m -> m.currency,
                            v -> v.isValid("currency", m -> Currency.fromCode(m.currency) != null))
                    .nonNull("amount", m -> m.amount,
                            v -> v.isValid("amount", m -> Double.parseDouble(m.amount) > 0))
                    .done();
        }
    }

    @Data
    @Builder
    private final static class TransferResp {
        private long id;
        private long fromAccountId;
        private long toAccountId;
        private MoneyTransferStatus status;
        private String statusDescription;

        public static TransferResp fromAccount(MoneyTransfer transfer) {
            return TransferResp.builder()
                    .id(transfer.getId())
                    .fromAccountId(transfer.getFromAccountId())
                    .toAccountId(transfer.getToAccountId())
                    .status(transfer.getStatus())
                    .statusDescription(transfer.getStatusDescription())
                    .build();
        }
    }
}
