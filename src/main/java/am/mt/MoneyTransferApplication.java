package am.mt;

import am.mt.api.rest.AccountsRestApi;
import am.mt.api.rest.TransfersRestApi;
import am.mt.core.services.impl.AccountManagementInMemoryService;
import am.mt.core.services.impl.IdGeneratorImpl;
import am.mt.core.services.impl.InMemoryTransfersRepository;
import am.mt.core.services.impl.MoneyTransferServiceImpl;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import spark.Spark;

import java.util.concurrent.atomic.AtomicBoolean;

public final class MoneyTransferApplication {

    private final AtomicBoolean stated = new AtomicBoolean();

    public static void main(String[] args) {
        int port = args.length == 1 ? Integer.parseInt(args[0]) : 8888;
        new MoneyTransferApplication().run(port);
    }

    public final void run(int port) {
        if (!stated.compareAndSet(false, true)) {
            throw new IllegalStateException();
        }

        Gson mapper = new GsonBuilder()
                .setLenient()
                .create();
        IdGeneratorImpl idGen = new IdGeneratorImpl();
        AccountManagementInMemoryService accountManagement = new AccountManagementInMemoryService(idGen);
        InMemoryTransfersRepository transfersRepository = new InMemoryTransfersRepository();
        MoneyTransferServiceImpl moneyTransfer = new MoneyTransferServiceImpl(idGen, accountManagement, transfersRepository);

        Spark.port(port);
        Spark.defaultResponseTransformer((model) -> model != null ? mapper.toJson(model) : null);
        new TransfersRestApi(mapper, accountManagement, moneyTransfer).initialize();
        new AccountsRestApi(mapper, accountManagement).initialize();
    }
}
