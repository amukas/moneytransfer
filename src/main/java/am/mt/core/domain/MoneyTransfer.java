package am.mt.core.domain;

import static am.mt.core.domain.MoneyTransferStatus.FAILED;
import static am.mt.core.domain.MoneyTransferStatus.SUCCEED;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;

@Data
@EqualsAndHashCode(of = "id")
@Builder(toBuilder = true)
public final class MoneyTransfer {
	private final long id;
	private final long fromAccountId;
	private final long toAccountId;
	@NonNull
	private final MoneyTransferStatus status;
	private final String statusDescription;

	public static class MoneyTransferBuilder {
		public MoneyTransfer failed(@NonNull String description) {
			return this.status(FAILED).statusDescription(description).build();
		}

		public MoneyTransfer succeed() {
			return this.status(SUCCEED).build();
		}
	}
}
