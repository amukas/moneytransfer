package am.mt.core.domain;

public enum MoneyTransferStatus {
	CREATED,
	IN_PROGRESS,
	SUCCEED,
	FAILED;
}
