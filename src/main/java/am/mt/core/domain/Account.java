package am.mt.core.domain;

import java.util.HashMap;
import java.util.Map;

import lombok.Builder;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Singular;

@Data
@EqualsAndHashCode(of = "id")
@Builder(toBuilder = true)
public final class Account {

	private final long id;
	private final String holder;
	@NonNull
	@Singular
	private Map<Currency, Long> balances;

	public long balanceFor(Currency currency) {
		Long rawBalance = this.balances.get(currency);
		if (rawBalance == null) {
			return 0L;
		}

		return rawBalance;
	}

	public boolean hasEnoughBalance(Currency currency, long amount) {
		return balanceFor(currency) >= amount;
	}

	public Account withdraw(Currency currency, long amount) {
		if (!hasEnoughBalance(currency, amount)) {
			throw new RuntimeException("Not enough balance");
		}

		Map<Currency, Long> updatedBalance = new HashMap<>(this.balances);
		updatedBalance.computeIfPresent(currency, (_c, balance) -> balance - amount);

		return this.toBuilder()
		           .balances(updatedBalance)
		           .build();
	}

	public Account deposit(Currency currency, long amount) {
		Map<Currency, Long> updatedBalance = new HashMap<>(this.balances);
		updatedBalance.compute(currency, (_c, balance) -> balance == null ? amount : balance + amount);

		return this.toBuilder()
		           .balances(updatedBalance)
		           .build();
	}
}
