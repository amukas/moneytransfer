package am.mt.core.domain;

import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public enum Currency {
    USD("USD"),
    EUR("EUR");

    private final String code;

    public static Currency fromCode(String code) {
        for (Currency each : values()) {
            if (each.code.equalsIgnoreCase(code)) {
                return each;
            }
        }
       return null;
    }
}
