package am.mt.core.services;

import am.mt.core.domain.Account;
import am.mt.core.domain.Currency;

public interface AccountManagementService {

	Account lockAccount(long accountId);

	void unlockAccount(long accountId);

	boolean accountExists(long accountId);

	Account findById(long accountId);

	Account createAccount(String accountHolder);

	/**
	 * Atomically updates multiple accounts.
	 */
	void updateAccount(Account... accounts);

	Account topUp(long accountId, Currency currency, long amount);
}
