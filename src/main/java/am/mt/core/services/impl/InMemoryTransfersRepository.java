package am.mt.core.services.impl;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import am.mt.core.domain.MoneyTransfer;
import am.mt.core.services.TransfersRepository;

public class InMemoryTransfersRepository implements TransfersRepository {

	private final Map<Long, MoneyTransfer> transfers = new ConcurrentHashMap<>();

	@Override
	public MoneyTransfer findById(long moneyTransferId) {
		return transfers.get(moneyTransferId);
	}

	@Override
	public void saveOrUpdate(MoneyTransfer moneyTransfer) {
		transfers.put(moneyTransfer.getId(), moneyTransfer);
	}
}
