package am.mt.core.services.impl;

import am.mt.core.domain.Account;
import am.mt.core.domain.Currency;
import am.mt.core.services.AccountManagementService;
import am.mt.core.services.IdGenerator;
import lombok.RequiredArgsConstructor;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

@RequiredArgsConstructor
public class AccountManagementInMemoryService implements AccountManagementService {

    private final IdGenerator idGen;
    private final Map<Long, Account> storage = new ConcurrentHashMap<>();
    private final Map<Long, Lock> locks = new ConcurrentHashMap<>();

    @Override
    public Account lockAccount(long accountId) {
        checkIfExist(accountId);
        locks.computeIfAbsent(accountId, (_id) -> new ReentrantLock()).lock();
        return storage.get(accountId);
    }

    @Override
    public void unlockAccount(long accountId) {
        checkIfExist(accountId);
        locks.computeIfAbsent(accountId, (_id) -> new ReentrantLock()).unlock();
    }

    @Override
    public boolean accountExists(long accountId) {
        return storage.containsKey(accountId);
    }

    @Override
    public Account findById(long accountId) {
        return this.storage.get(accountId);
    }

    @Override
    public Account createAccount(String accountHolder) {
        Account account = Account.builder()
                .id(idGen.nextId())
                .holder(accountHolder)
                .build();
        this.storage.put(account.getId(), account);
        return account;
    }

    @Override
    public void updateAccount(Account... accounts) {
        for (Account account : accounts) {
            this.storage.put(account.getId(), account);
        }
    }

    @Override
    public Account topUp(long accountId, Currency currency, long amount) {
        try {
            Account account = lockAccount(accountId);
            Account updatedAccount = account.toBuilder()
                    .balance(currency, account.balanceFor(currency) + amount)
                    .build();
            this.storage.put(updatedAccount.getId(), updatedAccount);
            return updatedAccount;
        } finally {
            unlockAccount(accountId);
        }
    }

    private void checkIfExist(long accountId) {
        if (!storage.containsKey(accountId)) {
            throw new RuntimeException("Account [" + accountId + "] does not exist.");
        }
    }
}
