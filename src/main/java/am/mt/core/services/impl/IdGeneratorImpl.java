package am.mt.core.services.impl;

import java.util.concurrent.atomic.AtomicLong;

import am.mt.core.services.IdGenerator;

public class IdGeneratorImpl implements IdGenerator {

	private final AtomicLong currentId = new AtomicLong(0);

	@Override
	public long nextId() {
		return currentId.incrementAndGet();
	}
}
