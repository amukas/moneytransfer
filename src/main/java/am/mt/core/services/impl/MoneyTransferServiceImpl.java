package am.mt.core.services.impl;

import am.mt.core.domain.Account;
import am.mt.core.domain.Currency;
import am.mt.core.domain.MoneyTransfer;
import am.mt.core.domain.MoneyTransfer.MoneyTransferBuilder;
import am.mt.core.domain.MoneyTransferStatus;
import am.mt.core.services.AccountManagementService;
import am.mt.core.services.IdGenerator;
import am.mt.core.services.MoneyTransferService;
import am.mt.core.services.TransfersRepository;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class MoneyTransferServiceImpl implements MoneyTransferService {

	private final IdGenerator idGenerator;
	private final AccountManagementService accountManagement;
	private final TransfersRepository transfersRepository;

	@Override
	public MoneyTransfer findById(long moneyTransferId) {
		return transfersRepository.findById(moneyTransferId);
	}

	@Override
	public MoneyTransfer transfer(long fromAccountId, long toAccountId, Currency currency, long amount) {
		MoneyTransfer transfer = withLocking(fromAccountId, toAccountId, (from, to, transferBuilder) -> {
			if (!from.hasEnoughBalance(currency, amount)) {
				return transferBuilder.failed("Not enough balance");
			}
			Account updatedFrom = from.withdraw(currency, amount);
			Account updatedTo = to.deposit(currency, amount);
			accountManagement.updateAccount(updatedFrom, updatedTo); // to updated atomically
			return transferBuilder.succeed();
		});

		transfersRepository.saveOrUpdate(transfer);
		return transfer;
	}

	private MoneyTransfer withLocking(long fromAccountId, long toAccountId, ProcessingLogic logic) {
		//to avoid dead locks, maintain same locking order
		boolean useReverseLock = fromAccountId > toAccountId;

		try {
			Account fromAccount;
			Account toAccount;
			if (useReverseLock) {
				toAccount = accountManagement.lockAccount(toAccountId);
				fromAccount = accountManagement.lockAccount(fromAccountId);

			} else {
				fromAccount = accountManagement.lockAccount(fromAccountId);
				toAccount = accountManagement.lockAccount(toAccountId);
			}

			MoneyTransferBuilder moneyTransfer = MoneyTransfer.builder()
			                                                  .id(idGenerator.nextId())
			                                                  .status(MoneyTransferStatus.CREATED)
			                                                  .fromAccountId(fromAccountId)
			                                                  .toAccountId(toAccountId);

			return logic.process(fromAccount, toAccount, moneyTransfer);
		} finally {
			if (useReverseLock) {
				accountManagement.unlockAccount(fromAccountId);
				accountManagement.unlockAccount(toAccountId);
			} else {
				accountManagement.unlockAccount(toAccountId);
				accountManagement.unlockAccount(fromAccountId);
			}
		}
	}

	@FunctionalInterface
	private interface ProcessingLogic {
		MoneyTransfer process(Account fromAccount, Account toAccount, MoneyTransferBuilder transfer);
	}
}
