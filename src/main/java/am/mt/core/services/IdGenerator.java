package am.mt.core.services;

public interface IdGenerator {

	long nextId();
}
