package am.mt.core.services;

import am.mt.core.domain.Currency;
import am.mt.core.domain.MoneyTransfer;

public interface MoneyTransferService {

	MoneyTransfer findById(long moneyTransferId);

	MoneyTransfer transfer(long fromAccountId, long toAccountId, Currency currency, long amount);
}
