package am.mt.core.services;

import am.mt.core.domain.MoneyTransfer;

public interface TransfersRepository {

	MoneyTransfer findById(long moneyTransferId);

	void saveOrUpdate(MoneyTransfer moneyTransfer);
}
