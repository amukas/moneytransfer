package am.mt.core.utils;

public interface WithAmountConversion {

	WithAmountConversion AmountConversion = new WithAmountConversion() {
	};

	default long toRawAmount(double amount) {
		return (long) (amount * 100);
	}

	default double fromRawAmount(long rawAmount) {
		return rawAmount / 100.0;
	}
}
